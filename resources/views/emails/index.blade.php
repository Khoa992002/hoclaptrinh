<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hóa Đơn Tính Tiền</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h2 {
            color: #333;
        }

        ul {
            list-style-type: none;
            padding: 0;
        }

        li {
            border-bottom: 1px solid #ccc;
            padding: 10px;
            margin-bottom: 20px;
        }

        .label {
            font-weight: bold;
            color: #555;
            margin-right: 10px;
        }

        .product-image {
            height: 150px;
            width: 150px;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .total-price {
            font-size: 18px;
            font-weight: bold;
            margin-top: 20px;
        }
    </style>
</head>
<body>

    <h2>Hóa Đơn Tính Tiền</h2>
  
    
    <ul>
        @forelse($data['orderDetails'] as $orderDetail)

            <li>
                <span class="label">Tên Sản phẩm:</span> {{ $orderDetail->name }} <br>
                <span class="label">Giá:</span> {{ $orderDetail->price }}$ <br>
                <span class="label">Số lượng:</span> {{ $orderDetail->quantity }} <br>
                <span class="label">Hình ảnh:</span> <img class="product-image" src="{{ asset($orderDetail->image) }}" alt="">

                <span class="label">Tổng số tiền:</span> {{ $orderDetail->total_price }}$ <br>
            </li>
        @empty
            <li>No order details available</li>
        @endforelse

        <p class="total-price">Tổng hóa đơn: {{ $data['totalPrice'] }}$</p>
    </ul>

</body>
</html>

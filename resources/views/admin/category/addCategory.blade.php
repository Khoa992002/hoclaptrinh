@extends('admin.layouts.app')
@section('content')


<div class="container-fluid">
                      @if(session('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                                {{session('success')}}
                            </div>
                        @endif

<div class="container-fluid">

 <form  method="post" action="category/add" class="form-horizontal form-material" enctype="multipart/form-data">
   @csrf
	<div class="form-group">       
        <label for="name">Title(*)</label>
        <input type="text" id="name" name="title">
    </div>
   <div class="form-group">
                
 
   <div class="form-group">
   	 <button  class="btn btn-success">thêm category</button>
   </div>
</form>	

 @if($errors->any())
                                    <div class="alert alert-danger alert-dismissible">
                                    
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                        @endif
</div>

@endsection    
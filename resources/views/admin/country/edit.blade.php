@extends('admin.layouts.app')
@section('content')

@if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
        {{ session('success') }}
    </div>
@endif

<form method="post">
    @csrf

    <label for="name">Title:</label>
    <input type="text" id="name" name="name" value="{{$data->name }}" required>

    <br>
    <button type="submit">chỉnh sữa</button>
</form>

@endsection
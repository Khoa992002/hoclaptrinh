@extends('admin.layouts.app')
@section('content')
<div class="container-fluid">
                      @if(session('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                                {{session('success')}}
                            </div>
                        @endif

<div class="container-fluid">

 <form  method="post"   action="" class="form-horizontal form-material" enctype="multipart/form-data">
   @csrf
	<div class="form-group">       
        <label for="name">Title(*)</label>
        <input type="text" id="name" name="title">
    </div>
   <div class="form-group">
                
   <input  class="btn btn-success" type="file" class="form-control form-control-line" name="image" >
  
   </div>

   <div class="form-group">
      <label for="description">Description:</label>
      <textarea type="description" name="description" class="form-control"></textarea>
   </div>

   <div class="form-group">

	<label>content</label>
	<textarea name="txtContent" class="form-control" id="demo"></textarea>
   </div>

   <div class="form-group">
   	 <button  class="btn btn-success">Create blog</button>
   </div>
</form>	

 @if($errors->any())
                                    <div class="alert alert-danger alert-dismissible">
                                    
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                        @endif
</div>
  

@endsection
@extends('frontend.layouts.app')
@section('content')

<form method="post">
						@CSRF
					<div class="replay-box">
						<div class="row">
							<div class="col-sm-12">
								<h2>Leave a replay</h2>
								<input type="hidden" name="id_blog" value="{{ $blog->id }}">
								<div class="text-area">
									<div class="blank-arrow">
										<label>Your Name</label>
									</div>
									<span>*</span>
									<textarea name="message" rows="11"></textarea>
									<<button type="submit" class="btn btn-primary">Post Comment</button>
								</div>
							</div>
						</div>
					</form>	


@endsection

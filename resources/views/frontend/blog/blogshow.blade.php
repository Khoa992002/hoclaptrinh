@extends('frontend.layouts.app')
@section('content')



<div class="col-sm-9">


	<form method="post">
    @csrf
    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>
        <div class="single-blog-post">
            <h3>{{ $blog->title ??'None'}}</h3>
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-user"></i> Mac Doe</li>
                    <li><i class="fa fa-clock-o"></i> {{ $blog->created_at }}</li>
                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                </ul>

                <div class="rate">
                    <div class="vote">
                        @for ($i = 1; $i <= 5; $i++)
                            @if ($average_rate >= $i)
                                <div class="star_{{ $i }} ratings_stars ratings_hover"><input value="{{ $i }}" type="hidden"></div>
                            @else
                                <div class="star_{{ $i }} ratings_stars"><input value="{{ $i }}" type="hidden"></div>
                            @endif
                        @endfor
                        <span class="rate-np">{{ round($average_rate, 1) }}</span>
                    </div>
                </div>

            </div>
            <a href="">
                <img src="{{ asset('admin/upload/blog/' . $blog->image ) }}" alt="{{ $blog->title }}">
            </a>
            <p>{{ $blog->description }}</p> <br>

            <div class="pager-area">
                <ul class="pager pull-right">
                    @if($prevPost)
                        <li><a href="{{ route('blogshow', ['id' => $prevPost->id]) }}">Previous</a></li>
                    @endif
                    @if($nextPost)
                        <li><a href="{{ route('blogshow', ['id' => $nextPost->id]) }}">Next</a></li>
                    @endif
                </ul>
            </div>

        </div>
    </div><!--/blog-post-area-->
</form>
			

					<div class="rating-area">
						<ul class="ratings">
							<li class="rate-this">Rate this item:</li>
							<li>
								<i class="fa fa-star color"></i>
								<i class="fa fa-star color"></i>
								<i class="fa fa-star color"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</li>
							<li class="color">(6 votes)</li>
						</ul>
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
					</div><!--/rating-area-->

					<div class="socials-share">
						<a href=""><img src="images/blog/socials.png" alt=""></a>
					</div><!--/socials-share-->

					<!-- <div class="media commnets">
						<a class="pull-left" href="#">
							<img class="media-object" src="images/blog/man-one.jpg" alt="">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Annie Davis</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<div class="blog-socials">
								<ul>
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-dribbble"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								</ul>
								<a class="btn btn-primary" href="">Other Posts</a>
							</div>
						</div>
					</div> --><!--Comments-->
					<div class="response-area">
						<h2>3 RESPONSES</h2>
						<ul class="media-list">
							@foreach($comments as $comment)
								@if($comment->level==0)
									<li class="media">
										
										<a class="pull-left" href="#">
											<img class="media-object" src="frontend/images/blog/man-two.jpg" alt="">
										</a>
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>"{{$comment->name}}"</li>
												<li><i class="fa fa-clock-o"></i> "{{$comment->created_at}}"</li>
												<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
											</ul>
											<p>"{{$comment->cmt}}"</p>
											<a  id="{{$comment->id}}"  class="btn btn-primary cmt" href="#"><i class="fa fa-reply"></i>Replay</a>
										</div>
									</li>
								
								 @endif
								@foreach($comments as $value)
								@if($value->level == $comment->id) 
									<li class="media second-media">
										<a class="pull-left" href="#">
											<img class="media-object" src="images/blog/man-three.jpg" alt="">
										</a>
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{$value->name}}</li>
												<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
												<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
											</ul>
											<p>{{$value->cmt}}</p>
											<a  id="{{$value->id}}"  class="btn btn-primary cmt" ><i class="fa fa-reply"></i>Replay</a>
										</div>
									</li>
								
								@endif

							
							  @endforeach
							@endforeach
						</ul>					
					</div><!--/Response-area-->
					 <!-- gửi cmt sang router bằng submitcomenr -->

					<form method="post" action="{{ url('/blog/cmt') }}">
						 @if(session('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                                {{session('success')}}
                            </div>
                        @endif
						@CSRF
						<div class="replay-box">
								<div class="row">
									<div class="col-sm-12">
										<h2>Leave a replay</h2>
										<input type="hidden" name="id_blog" value="{{ $blog->id }}">
										<div class="text-area">
											<div class="blank-arrow">
												<label>Your Name</label>
											</div>
											<span>*</span>
											<textarea name="message" rows="11"></textarea>
											<p class="err1"></p>
											 <!-- xử lý ẩn để cho level = 0 -->
											<input type="hidden" name="level" class="level" value="0">
											<button type="submit" class="btn btn-primary">Post Comment</button>
									</div>
								</div>
						</div>

						
					</form>	
					</div><!--/Repaly Box-->
			  </div>	

			<!-- 	- tao 1 form bao quanh
				- dung js click button 
				  + kiem tra login chưa
				  + kiem tra nhâp cmt chưa (val())
				  + nhow la cho return false, de dừng lai form
				  + 2 cái ok thi cho return true, co action (bo router vao action) -->


	    
<script>
    // xử lý js đánh giá
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // vote
        $('.ratings_stars').hover(
            // Handles the mouseover
            function () {
                $(this).prevAll().andSelf().addClass('ratings_hover');
                // $(this).nextAll().removeClass('ratings_vote'); 
            },
            function () {
                $(this).prevAll().andSelf().removeClass('ratings_hover');
                // set_votes($(this).parent());
            }
        );

        $('.ratings_stars').click(function () {

            var id_blog = {{$blog->id}};

            // // goi php vao 
            var checkLogin = "{{Auth::check()}}";
            // alert()

            if (checkLogin) {
                var rate = $(this).find("input").val();
                alert(rate);
                if ($(this).hasClass('ratings_over')) {
                    $('.	').removeClass('ratings_over');
                    $(this).prevAll().andSelf().addClass('ratings_over');
                } else {
                    $(this).prevAll().addBack().addClass('ratings_over');
                }

                // phan tich xem rate co gi? de tao table co đung?
                // id, rate, id_blog, id_user
                // dung ajax gui qua controller va insert table rate
                $.ajax({
                    type: 'POST',
                    url: '{{url("/blog/rate")}}',
                    data: {
                        rate: rate,
                        id_blog: id_blog,
                    },
                    success: function (data) {
                        console.log(data.success);
                    }
                });

            } else {
                alert("vui long login de danh gia rate");
            }

        });

        $("form").submit(function () {
           
            var getMess = $("textarea[name='message']").val();
            var checkLogin = "{{Auth::Check()}}";
            
            var id_blog = {{ $blog->id }};

            var x = 1;
             if (checkLogin) {
               if (getMess == "") {
                $("p.err1").text("vui lòng nhập cmt")
                x = 2;
                return false;
            } else {
                $("p.err1").text("")
            }
            } else {
                alert("vui lòng đăng nhập để cmt");
                x = 2;
            }
           
            if (x === 1) {
                return true;
            }

            return false;

        });
        $(".cmt").click(function(){
            var idCm = $(this).attr('id');
            $("input.level").val(idCm);

            
    
       });

    });
</script>


@endsection


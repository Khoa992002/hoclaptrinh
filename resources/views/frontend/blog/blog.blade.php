@extends('frontend.layouts.app')
@section('content')

 <div class="col-sm-9">

                    <div class="blog-post-area">
                         @foreach($data as $key => $item)
                        <h2 class="title text-center">Latest From our Blog</h2>
                        <div class="single-blog-post">
                            <h3>{{$item->title}}</h3>
                            <div class="post-meta">
                                <ul>
                                    <li><i class="fa fa-user"></i> Mac Doe</li>
                                    <li><i class="fa fa-clock-o"></i> {{$item->created_at}}</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <div class="rate">
                                    
                                     
                                      <div class="vote">
                                        <div class="star_1 ratings_stars"><input value="1" type="hidden"></div>
                                        <div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
                                        <div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
                                        <div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
                                        <div class="star_5 ratings_stars"><input value="5" type="hidden"></div>
                                        <span class="rate-np"></span>
                                      </div>
                                      
                                      
                                </div>
                            </div>
                            <a href="">
                                 <img src="{{ asset('admin/upload/blog/' . $item->image) }}" alt="{{ $item->title }}">
                            </a>
                            <p>{{$item->description}}</p>
                            <a  class="btn btn-primary" href="{{ route('blogshow', ['id' => $item->id]) }}">Read More</a>
                        </div>
                        
                        
                        
                        @endforeach
                    </div>
                    <div class="pagination-area">
                            <ul class="pagination">
                               {!! $data->links('pagination::bootstrap-4') !!}  
                            </ul>
                        </div>

                </div>

    <script src="js/jquery-1.9.1.min.js"></script>             
    <script>
        //xử lý js đánh giá
         $(document).ready(function(){

            $.ajaxSetup({
                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //vote
            $('.ratings_stars').hover(
                // Handles the mouseover
                function() {
                    $(this).prevAll().andSelf().addClass('ratings_hover');
                    // $(this).nextAll().removeClass('ratings_vote'); 
                },
                function() {
                    $(this).prevAll().andSelf().removeClass('ratings_hover');
                    // set_votes($(this).parent());
                }
            );

            $('.ratings_stars').click(function(){

                 var id_blog = {{$item->id}};


                // // goi php vao 
                var checkLogin = "{{Auth::Check()}}";
                // alert()

                if(checkLogin){
                    var rate =  $(this).find("input").val();
                    alert(rate);
                    if ($(this).hasClass('ratings_over')) {
                        $('.    ').removeClass('ratings_over');
                        $(this).prevAll().andSelf().addClass('ratings_over');
                    } else {
                        $(this).prevAll().andSelf().addClass('ratings_over');
                    }

                    // phan tich xem rate co gi? de tao table co đung?
                    // id, rate, id_blog, id_user
                    // dung ajax gui qua controller va insert table rate
                     $.ajax({
                       type:'POST',
                       url:'{{url("/blog/rate")}}',
                       data:{
                        rate:rate,
                        id_blog:id_blog,
                        },
                       success:function(data){
                          console.log(data.success);
                       }
                    });


                }else{
                    alert("vui long login de danh gia rate");
                }
                
            });
        });
    </script>    

@endsection                                    
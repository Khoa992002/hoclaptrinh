@extends('frontend.layouts.app')
@section('content')

<section>

		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Account</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">account</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">My product</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					
						
					</div>
				</div>
				<div class="col-sm-9">
					<div class="table-responsive cart_info">
						<table class="table table-condensed">
							<thead>
								<tr class="cart_menu">
									<td class="id">id</td>
									<td class="image">Image</td>
									<td class="description">Description</td>
									<td class="price">Price</td>
									<td class="total">Total</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
                 
               
								@foreach($product as $product)
								<?php
                // Giải mã thuộc tính hinhanh thành mảng
                 $hinhAnhArray = json_decode($product->hinhanh, true);
                 ?>

					      
								
							
								<tr>
									<td class="cart_description">
										<h4><a href=""></a>{{$product->id}}</h4>
										
									</td>
									<td class="cart_product">
										<a href=""><img style="width: 80px" src="{{ asset('upload/product/' . $hinhAnhArray[0] ) }}" alt="{{ $product->name }}"></a>
									</td>
									<td class="cart_description">
										<h4><a href="">{{$product->name}}</a></h4>
										
									</td>
									<td class="cart_price">
										<p>{{$product->price}}</p>
									</td>
									
									<td class="cart_delete">
										<a class="cart_quantity_delete" href="{{ route('deletepro', ['id' => $product->id]) }}"><i class="fa fa-times"></i></a>

									</td>
									 <td><a href="{{ route('editproduct', ['id' => $product->id]) }}">Sửa</a></td>
								</tr>
								@endforeach
								






							
							</tbody>
							<tfoot>
                                           <td colspan="8">
                                             <a href="/add"><button class="btn btn-default get" id="button" method="post" >ADD NEW</button></a>
                                            </td>
                                        </tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>


@endsection                                    

@extends('frontend.layouts.app')
@section('content')

<div class="col-sm-9">
		<div class="blog-post-area">
			@if(session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
        {{ session('success') }}
    </div>
@endif


		  <form method="post" action="{{url('/edit/new/'.$data->id)}}" enctype="multipart/form-data">

            @csrf

            <div class="form-group"><!-- Added form-group for better spacing -->
                <input type="text" class="form-control" name="name" value="{{$data->name}}" placeholder="Name" />
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="price" value="{{$data->price}}" placeholder="Price" />
            </div>

            <div class="form-group">
                <select class="form-control form-control-line" value="{{$data->name}}" name="category_id">
                    <option value="" disabled selected>{{$data->category_id}}</option>
                    <option value="1">Nước Hoa</option>
                    <option value="2">Quần Áo</option>
                </select>
            </div>

            <div class="form-group">
                <select class="form-control form-control-line" value="" name="brand_id">
                    <option value="{{$data->name}}" disabled selected>{{$data->brand_id}}</option>
                    <option value="1">Gucci</option>
                    <option value="2">Dior</option>
                </select>
            </div>

            <div class="form-group">
                <select class="form-control form-control-line" name="sale">
                    <option value="{{$data->sale}}" disabled selected>{{$data->sale}}</option>
                    <option value="0">0%</option>
                    <option value="10">10%</option>
                    <option value="20">20%</option>
                    <option value="30">30%</option>
                    <option value="40">40%</option>
                    <option value="50">50%</option>
                </select>
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="company" placeholder="Company profile" value="{{$data->company}}" />
            </div>

            <div class="form-group">
                <input class="form-control form-control-line" type="file" multiple name="avatar[]">
            </div>
             @foreach(json_decode($data->hinhanh, true)   as $image)
             
                <li>
                   <img src="{{ asset('upload/product/' . $image) }}" style="width: 50px;" />
                   <input type="checkbox" class="checkbox" name="imagesToDelete[]" value="{{ $image }}" />
               </li>
             
             @endforeach    
       
            <div class="form-group">
                <textarea class="form-control" name="detail" placeholder="Detail" value="{{$data->detail}}">{{$data->detail}}</textarea>
            </div>

            <button type="submit" class="btn btn-default">SEND</button>
        </form>
		</div>
</div>



@endsection     
                              

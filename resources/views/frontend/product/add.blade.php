@extends('frontend.layouts.app')

@section('content')
<div class="col-sm-6 offset-sm-3"><!-- Adjusted column width and added offset -->
    @if(session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
            {{ session('success') }}
        </div>
    @endif

    <div class="signup-form"><!-- sign up form -->
        <h2>New User Signup!</h2>
        <form method="post" action="{{ url('/add/new') }}" enctype="multipart/form-data">

            @csrf

            <div class="form-group"><!-- Added form-group for better spacing -->
                <input type="text" class="form-control" name="name" placeholder="Name" />
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="price" placeholder="Price" />
            </div>

            <div class="form-group">
                <select class="form-control form-control-line" name="category_id">
                    <option value="" disabled selected>Please choose a category</option>
                    <option value="1">Nước Hoa</option>
                    <option value="2">Quần Áo</option>
                </select>
            </div>

            <div class="form-group">
                <select class="form-control form-control-line" name="brand_id">
                    <option value="" disabled selected>Please choose a brand</option>
                    <option value="1">Gucci</option>
                    <option value="2">Dior</option>
                </select>
            </div>

            <div class="form-group">
                <select class="form-control form-control-line" name="sale">
                    <option value="" disabled selected>Sale</option>
                    <option value="0">0%</option>
                    <option value="10">10%</option>
                    <option value="20">20%</option>
                    <option value="30">30%</option>
                    <option value="40">40%</option>
                    <option value="50">50%</option>
                </select>
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="company" placeholder="Company profile" />
            </div>

                <div class="form-group">
                    <input class="form-control form-control-line" type="file" multiple name="avatar[]">
                </div>

            <div class="form-group">
                <textarea class="form-control" name="detail" placeholder="Detail"></textarea>
            </div>

            <button type="submit" class="btn btn-default">SEND</button>
        </form>

        @if($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div><!-- /sign up form -->
</div>
@endsection


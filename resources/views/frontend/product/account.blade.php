@extends('frontend.layouts.app')
@section('content')


		<div class="container">

			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Account</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="">account</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="{{url('/product/myproduct')}}">My product</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					
						
					</div>
				</div>
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Update user</h2>
						 <div class="signup-form"><!--sign up form-->
						<h2>user update</h2>
						 @if(session('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                                {{session('success')}}
                            </div>
                        @endif
						<form method="post"  class="form-horizontal form-material" action="{{ url('/account/update') }}" enctype="multipart/form-data">
							@csrf
							<input type="text" placeholder="Name" name="name" value="{{$member->name}}" />
							<input type="email" placeholder="Email Address" name="email" value="{{$member->email}}"/>
							<input type="password" placeholder="Password" name="password" value=""/>
							<input type="address" placeholder="address"  name="address"  value="{{$member->address}}"/>
							<input type="file" class="form-control form-control-line" name="avatar" >
							<button type="submit" class="btn btn-default">update account</button>
						</form>
					</div>
					</div>
				</div>
			</div>
		</div>
	

@endsection                                    

@extends('frontend.layouts.app')
@section('content')	
	<div class="col-sm-4 col-sm-offset-1">
		  @if(session('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                                {{session('success')}}
                            </div>
                        @endif
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form action="#" method="post">
							@csrf
							<input type="text" name="email" placeholder="Email" />
							<input type="text" name="password" placeholder="Password" />
							<span>
								<input type="checkbox" class="checkbox" name="remember_me" id="remember_me"> 
								remember_me
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
						
					</div><!--/login form-->
				</div>
				
				
@endsection				
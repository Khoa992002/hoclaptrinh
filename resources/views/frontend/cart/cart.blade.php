@extends('frontend.layouts.app')
@section('content')

<section id="cart_items">


		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@foreach ($cart as $cart)
						<tr>
							<td class="cart_product">
								<a href=""><img style="height:150px; width:150px;" src="{{ $cart->image }}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$cart->name}}</a></h4>
								<p class="cart_id">{{$cart->id}}</p>
							</td>
							<td class="cart_price">
								<p>{{$cart->price}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" > + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{ $cart->quantity }}" autocomplete="off" size="2">
									<a class="cart_quantity_down" > - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">{{$cart->total_price}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" data-cart-id="{{ $cart->id }}" ><i class="fa fa-times"></i></a>
							</td>
						</tr>
                            @endforeach
										
						
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>$59</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{ $StotalPrice }}</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="{{url('checkout')}}">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
<script type="text/javascript">
    function updateCart(quantity_update, cartId, action) {
    // Thiết lập Ajax header
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Gửi yêu cầu Ajax để cập nhật số lượng
    $.ajax({
        type: "POST",
        url: "/Cart/capnhat",
        data: {
            quantity_update: quantity_update,
            cartId: cartId,
            action: action
        },
        success: function(response) {
            console.log("Cart updated successfully");
            // Thực hiện các hành động khác nếu cần
        },
        error: function(error) {
            console.error("Error updating cart:", error);
        }
    });
}
 function removeProduct(cartId) {	
        // Thiết lập Ajax header
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "/cart/remove",
            data: {
                cartId: cartId,
            },
            success: function(response) {
                console.log("Product removed successfully");
                $(".cart_id:contains('" + cartId + "')").closest("tr").remove();
                // Thực hiện các hành động khác nếu cần
            },
            error: function(error) {
                console.error("Error removing product:", error);
            }
        });
    }


$(document).ready(function() {
    // Xử lý khi click nút tăng số lượng
    $(".cart_quantity_up").click(function() {
        var currentQuantityInput = $(this).next(".cart_quantity_input");
        var price = parseFloat($(this).closest("tr").find(".cart_price p").text());
        var cartId = $(this).closest("tr").find(".cart_id").text();
        var currentQuantity = parseInt(currentQuantityInput.val());

        currentQuantity++;
        currentQuantityInput.val(currentQuantity);

        var newTotal = currentQuantity * price;
        $(this).closest("tr").find(".cart_total_price").text(newTotal);

        // Gửi yêu cầu Ajax để cập nhật số lượng
        updateCart(currentQuantity, cartId, "1");
    });

    // Xử lý khi click nút giảm số lượng
    $(".cart_quantity_down").click(function() {
        var currentQuantityInput = $(this).prev(".cart_quantity_input");
        var price = parseFloat($(this).closest("tr").find(".cart_price p").text());
        var cartId = $(this).closest("tr").find(".cart_id").text();
        var currentQuantity = parseInt(currentQuantityInput.val());

        if (currentQuantity > 1) {
            currentQuantity--;
        } else {
            removeProduct(cartId);
            $(this).closest("tr").remove();
            return;
        }

        currentQuantityInput.val(currentQuantity);

        var newTotal = currentQuantity * price;

        $(this).closest("tr").find(".cart_total_price").text(newTotal);

        // Gửi yêu cầu Ajax để cập nhật số lượng
        updateCart(currentQuantity, cartId, "2");
    });

      $(".cart_quantity_delete").click(function() {
            // Lấy giá trị cartId từ thuộc tính data
            var cartId = $(this).data("cart-id");

            // Gọi hàm removeProduct để xóa sản phẩm
            removeProduct(cartId);
        });
});

</script>

	

@endsection  
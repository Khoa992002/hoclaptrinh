@extends('frontend.layouts.app')
@section('content')

 <div class="features_items">
	<h2 class="title text-center">Kết quả tìm kiếm cho </h2>

						 @foreach($results as $product)
						 <?php 
                          //giải mã thuộc tính hình ảnh thành mảng
						  $hinhAnhArray = json_decode($product->hinhanh, true);
						 ?>
	<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="{{ asset('upload/product/' . $hinhAnhArray[0] ) }}" alt="{{ $product->name }}">

										<h2>{{$product->price}}</h2>
										<p>{{$product->name}}</p>
										<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2>{{$product->price}}</h2>
											<p>{{$product->name}}</p>

											 <a href="{{ url('/product/productdetail/' . $product->id) }}">Chi tiết sản phẩm</a>
                                              <!-- xử lý nút add to cart -->
											  <a class="btn btn-default add-to-cart" data-name="{{$product->name}}" data-id="{{$product->id}}" data-price="{{$product->price}}" data-image="{{ asset('upload/product/' . $hinhAnhArray[0] ) }}}">
                                              <i class="fa fa-shopping-cart"></i>Add to cart1
                                              </a>

										</div>
									</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
									</ul>
								</div>
							</div>
						</div>
						@endforeach
</div>

@endsection  
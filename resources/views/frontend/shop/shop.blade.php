@extends('frontend.layouts.app')
@section('content')

<div class="container">
    <form method="post" action="{{ url('shop/ketqua') }}" enctype="multipart/form-data">
    	@csrf

        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                </div>
            </div>
           <div class="col-md-2">
            <div class="form-group">
              <select class="form-control form-control-line" name="price">
              <option value="" disabled selected>Choose price</option>
              <option value="0-100"><100</option>
              <option value="100-500">100-500</option>
              <option value="500-1000">500-1000</option>	
             
           </select>
          </div>
           </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control form-control-line" name="category_id">
                        <option value="" disabled selected>category</option>
                        <option value="1">Nước Hoa</option>
                        <option value="2">Quần Áo</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control form-control-line" name="brand_id">
                        <option value="" disabled selected> brand</option>
                        <option value="1">Gucci</option>
                        <option value="2">Dior</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control form-control-line" name="status">
                        <option value="" disabled selected>Cstatus</option>
                        <option value="0">0%</option>
                        <option value="10">10%</option>
                        <option value="20">20%</option>
                        <option value="30">30%</option>
                        <option value="40">40%</option>
                        <option value="50">50%</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>


</div>






@endsection  
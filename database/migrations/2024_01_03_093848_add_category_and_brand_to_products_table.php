<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
        $table->bigInteger('category_id')->unsigned();
        $table->bigInteger('brand_id')->unsigned();

        // Thêm khoá ngoại
        $table->foreign('category_id')->references('id')->on('categories');
        $table->foreign('brand_id')->references('id')->on('brands');
    });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
};

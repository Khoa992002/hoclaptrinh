<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Rate;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;


class BlogController2 extends Controller
{

     
    
    
     public function GetBlog()
    {
        //xu ly nhu the nay la vua phan trang vua lay du lieu luon
        $data = Blog::paginate(3);


        return response()->json([
            'blog' => $data
        ]);
    }



public function GetDlog($id)  
{
    //dùng hàm average để tính trung bình
    $average_rate = Rate::where('id_blog', $id)->avg('rate');
    $comments = Comment::where('idblog', $id)->get();        
    $blog = Blog::find($id);
    $nextPost = Blog::where('id', '>', $id)->orderBy('id', 'asc')->first();
    $prevPost = Blog::where('id', '<', $id)->orderBy('id', 'desc')->first();

    // Tạo một mảng chứa dữ liệu bạn muốn trả về
    $data = [
        'blog' => $blog,
        'nextPost' => $nextPost,
        'prevPost' => $prevPost,
        'average_rate' => $average_rate,
        'comments' => $comments,
    ];

    // Trả về dữ liệu dưới dạng JSON
    return response()->json($data);
}



     public function rateBlog(Request $request){
       // kiem tra du lieu ajax da sang chua echo 11;

        if(Auth::check()){
            
            $rate= new Rate();
            $rate->rate = $request->input('rate');
            $rate->id_blog = $request->input('id_blog');
            $rate->id_user = Auth::id(); 
            $rate->save();
                return response()->json(['success' => 'Rating saved successfully']);
             } else {
                return response()->json(['error' => 'User not authenticated'], 401);
             }
    }


public function PostCmt(Request $request)
{
         //lây tất cả dữ liệu
        $data = $request->all();

        
       

        // Create a new Comment instance
        $comment = new Comment();

        // Assign values to the comment object
        $comment->cmt = $request->input('message');
        $comment->idblog = $request->input('id_blog');
        $comment->avatar = Auth()->user()->avatar;
        $comment->name = Auth()->user()->name;
        $comment->iduser = Auth::id();
        $comment->level =$request->input('level');
   
        // Save the comment to the database
        $comment->save();
         return redirect()->back()->with('success', __('cmt thanh cong.'));
    }
   




  //xử lý repcomt
 


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Blog;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\api\MemberRequest;
use App\Http\Requests\api\LoginRequest;

class MemberController extends Controller
{
       

    // Display registration form
    public function GetRegister()
    {
        return view('frontend/member/register');
    }

    // Process registration form data
    public function PostRegister(MemberRequest $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $phone = $request->input('phone');
        $address = $request->input('address');
        $avatar = $request->file('avatar');

        $user = new User();

        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->phone = $phone;
        $user->address = $address;
        $user->level = 0;

        $imageName = '';
        if ($avatar) {
            $imageName = $avatar->getClientOriginalName();
            $avatar->move(('admin/upload/blog'), $imageName);
            $user->avatar = $imageName;
        }

        $user->save();

        return redirect()->back()->with('success', __('đăng ký thành công.'));
    }

    // Display login form
    public function GetLogin()
    {
        return view('frontend/member/login');
    }

    // Process login form data
     public function PostLogin(LoginRequest $request)
{
    $login = [
        'email' => $request->email,
        'password' => $request->password,
        'level' => 0
    ];

    $member = $request->remember_me;

    if (Auth::attempt($login, $member)) {
        // Lấy token sau khi đăng nhập thành công (nếu bạn đang sử dụng xác thực token)
        $token = auth()->user()->createToken('AuthToken')->accessToken;

        return response()->json([
            'success' => 'success',
            'token' => $token, 
            'Auth' => Auth::user()
        ]);
    } else {
        return response()->json([
            'response' => 'error',
            'errors' => ['errors' => 'invalid email or password'],
        ]);
    }
}

    public function logout(Request $request)
{
    Auth::logout();
    return redirect()->back()->with('success', __('đăng xuất thành công')); // Điều hướng về trang chủ hoặc trang nào đó sau khi đăng xuất
}
       

    // Other resource methods (create, store, show, edit, update, destroy) are not implemented in the provided code snippet.
}

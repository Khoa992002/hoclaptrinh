<?php
namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use Image;

class ProductController extends Controller
{
    public function GetProduct(){
         // Lấy thông tin người dùng hiện tại
        $userId = Auth::user()->id;     
        // Thực hiện truy vấn để lấy thông tin người dùng từ bảng 'users'

       $product = Product::where('user_id', $userId)->get();
       $productId=Product::pluck('id')->toArray();

      // $getProducts = Product::find($productId)->toArray();
      // dd($getProducts);
      // $getArrImage = json_decode($getProducts['hinhanh'], true); 
       
       
        return view('frontend/product/myproduct',compact('product'));
    }
    public function AddProduct(){
        return view('frontend/product/add');
    }
 public function PostProduct(Request $request)
{
    $user = Auth::user();
    if($user){
        //$data=$request->all();
        //dd($data);

        $request->validate([
            'filename.*' => 'image|mimes:jpeg,png,jpg,gif|max:1024', // Max 1MB for each image
        ]);
          
          $data = []; // Khai báo mảng $data trước khi sử dụng
        if ($request->hasFile('avatar')) {

            foreach ($request->file('avatar') as $image) {
                $name = $image->getClientOriginalName();
                $name_2 = "hinh85_".$image->getClientOriginalName();
                $name_3 = "hinh200_".$image->getClientOriginalName();

                //$image->move('upload/product/', $name);
                
                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name_2);
                $path3 = public_path('upload/product/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                Image::make($image->getRealPath())->resize(329, 380)->save($path3);
                
                $data[] = $name;


            }
        }
      // dd($data);
      

                // Thêm thông tin hình ảnh và sản phẩm vào bảng products
                $product = new Product();
                $product->user_id = $user->id; 
                $product->name = $request->input('name');
                $product->price = $request->input('price');
                $product->category_id =$request->input('category_id');
                $product->brand_id = $request->input('brand_id');
                $product->sale = $request->input('sale');
                $product->company = $request->input('company');
                $product->detail = $request->input('detail');
                $product->hinhanh = json_encode($data); // Lưu tên hình ảnh gốc hoặc đường dẫn đầy đủ tùy thuộc vào yêu cầu
                // Kiểm tra sản phẩm trước khi lưu
                dump($product);
                //exit;
                $product->save();

        // Redirect to the list of products or perform any other action
        return redirect()->back()->with('success', 'Nhập sản phẩm thành công');
    } else {
        return redirect()->back()->withErrors('Người dùng chưa đăng nhập');
    }
}












 public function GetEditpro($id){
    $data = Product::where('id', $id)->first();
   
     return view('frontend/product/editproduct',compact('data'));
 }
 public function EditPro(Request $request, $id )
 {
 
    $data=$request->all();
    //dd($data);
    $product = Product::find($id);


     // Lấy hình ảnh từ trong lish cũ

    $oldImages = json_decode($product->hinhanh, true);


    // Lấy danh sách hình ảnh muốn xoá từ form
    $imagesToDelete = $request->input('imagesToDelete', []);

    //lặp qua danh sách hình ảnh muốn xóa và xóa khỏi lish cũ
    foreach($imagesToDelete as $image){
        $index= array_search($image,$oldImages);
        //dd($index);
        if($index !==false){
            unset($oldImages[$index]);  
        }
    }
    //lấy danh sách hình ảnh mới từ form sang
    $newImages=$request->file('avatar');
    //kết hợp danh sách hình ảnh cũ và hình ảnh mới

    $mergedImages = array_merge($oldImages, $newImages);
    //kiểm tra tổng hình ảnh có vượt uqas 3 không
      if (count($mergedImages) > 3) {
        return redirect()->back()->with('success', 'Tổng số hình ảnh không được vượt quá 3.');
    }

      // Đặt lại key của mảng để có key liên tục
    $mergedImages = array_values($mergedImages);    
    // Cập nhật trường 'hinhanh' với danh sách hình ảnh mới
    $product->hinhanh = json_encode($mergedImages);

    


    
    $product->name=$request->input('name');
    $product->price=$request->input('price');
    $product->sale=$request->input('sale');
    $product->company=$request->input('company'); 
    $product->detail=$request->input('detail');  

   
    $product->save();    

      return redirect()->back()->with('success', __('chinh sua san pham thanh cong.'));


 }


 public function Deletepro($id){
      // Tìm sản phẩm theo ID
    $product = Product::find($id);

    // Kiểm tra xem sản phẩm có tồn tại không
    if (!$product) {
        return redirect()->back()->withErrors('Sản phẩm không tồn tại.');
    }

    // Thực hiện xóa sản phẩm
    $product->delete();
    

    // Chuyển hướng hoặc xử lý kết quả
    return redirect()->back()->with('success', 'Sản phẩm đã được xóa thành công.');
 }
   public function GetTimkiem(Request $request)
    {
        $searchTerm = $request->input('name');

        $products = Product::where('name', 'like', '%' . $searchTerm . '%')->get();
        
        return view('frontend.seague.seague', ['products' => $products, 'searchTerm' => $searchTerm]);
    }
}

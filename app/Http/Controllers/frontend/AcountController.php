<?php

namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\File;
use Image;


class AcountController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function GetAccount()
    {
        // Lấy thông tin người dùng hiện tại
        $user = Auth::user();     
        // Thực hiện truy vấn để lấy thông tin người dùng từ bảng 'users'  
        $member=User::where('id',$user->id)->first();
        
        return view('frontend/product/account',compact('member'));
    }


   
    public function UpdateAccount(Request $request)
    {
         //lây tất cả dữ liệu
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $data = $request->all();
        
        $file = $request->avatar;
        

         if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }

        
        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
       
        if ($user->update($data)) {
            if(!empty($file)){
                $file->move('upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', __('Update profile success.'));
        } else {
            return redirect()->back()->withErrors('Update profile error.');
        }
    }
    
}

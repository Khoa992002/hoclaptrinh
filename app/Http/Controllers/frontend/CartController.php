<?php

namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
   public function GetCart()
   {
     if (Auth::check()) {
        // Lấy ID người dùng hiện tại nếu đã đăng nhập
        $userId = Auth::id();

        // Lấy thông tin giỏ hàng của người dùng hiện tại
        $cart2 = session()->get('cart', []);
        $StotalPrice = OrderDetail::where('order_id', $userId)->sum('total_price');
        
      
        $cart = OrderDetail::where('order_id', $userId)->get();

        // Trả về view hiển thị giỏ hàng với dữ liệu
        return view('frontend.cart.cart', compact('cart','cart2','StotalPrice'));
    } else {
        // Nếu người dùng chưa đăng nhập, có thể chuyển hướng hoặc hiển thị thông báo
        return redirect()->route('login')->with('message', 'Vui lòng đăng nhập để xem giỏ hàng.');
    }
   }
public function PostCart(Request $request)
{
   
 
    // Lấy ID người dùng hiện tại nếu đã đăng nhập
    $userId = Auth::id();

    // Lấy thông tin giỏ hàng của người dùng hiện tại
    $giohang = OrderDetail::where('product_id', $request->input('productId'))
                          ->where('order_id', $userId)
                          ->first();


    // Tạo mới đối tượng nếu sản phẩm chưa tồn tại trong giỏ hàng
    if (!$giohang) {
        $giohang = new OrderDetail();
        $giohang->order_id = $userId;
        $giohang->image = $request->input('productImage');
        $giohang->price = $request->input('productPrice');
        $giohang->name = $request->input('productName');
        $giohang->quantity = 1;
        $giohang->product_id = $request->input('productId');

        $giohang->save();
    } else {
        // Tăng số lượng nếu sản phẩm đã tồn tại trong giỏ hàng
        $giohang->quantity++;
        
    }
     // Tính toán và cập nhật giá trị cho cột total_price
    $giohang->total_price = $giohang->quantity * $giohang->price;

    // Lưu giỏ hàng
    $giohang->save();
}

public function PCart(Request $request){
      $data = $request->session()->all();
        

    $cartId = $request->input('cartId');
    $quantityUpdate = $request->input('quantity_update');

    // Lấy thông tin sản phẩm từ giỏ hàng
    $cartItem = OrderDetail::find($cartId);

    if (!$cartItem) {
        return response()->json(['error' => 'sản phảm không tioofn tại'], 404);
    }

    // Kiểm tra số lượng
    if ($quantityUpdate < 1) {
        // Nếu số lượng là 1 hoặc ít hơn, xóa sản phẩm khỏi giỏ hàng
        $cartItem->delete();
        return response()->json(['success' => 'Product removed from cart']);
    }

    // Ngược lại, cập nhật số lượng sản phẩm
    $cartItem->quantity = $quantityUpdate;
    $cartItem->total_price = $quantityUpdate * $cartItem->price;
    $cartItem->save();

    return response()->json(['success' => 'Cart updated successfully']);


}
public function RCart(Request $request)
{
   $cartId = $request->input('cartId');

    // Lấy thông tin sản phẩm từ giỏ hàng
    $cartItem = OrderDetail::find($cartId);

    if (!$cartItem) {
        return response()->json(['error' => 'Product not found'], 404);
    }

    // Xóa sản phẩm khỏi giỏ hàng
    $cartItem->delete();

    return response()->json(['success' => 'Product removed from cart']);
    }

 
}




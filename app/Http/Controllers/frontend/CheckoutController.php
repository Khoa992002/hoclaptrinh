<?php
namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class CheckoutController extends Controller
{
      public function GetCheckout()
      {
         if (Auth::check()) {
        // Lấy ID người dùng hiện tại nếu đã đăng nhập
        $userId = Auth::id();

        // Lấy thông tin giỏ hàng của người dùng hiện tại
        $cart2 = session()->get('cart', []);
         $StotalPrice = OrderDetail::where('order_id', $userId)->sum('total_price');
        
      
        $cart = OrderDetail::where('order_id', $userId)->get();

        // Trả về view hiển thị giỏ hàng với dữ liệu
        return view('frontend.checkout.checkout', compact('cart','cart2','StotalPrice'));
    } else {
        // Nếu người dùng chưa đăng nhập, có thể chuyển hướng hoặc hiển thị thông báo
        return redirect()->route('login')->with('message', 'Vui lòng đăng nhập để xem giỏ hàng.');
    }
      }  
}

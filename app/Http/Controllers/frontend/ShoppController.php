<?php
namespace App\Http\Controllers\frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class ShoppController extends Controller
{
    public function GetShop() 
    {
        return view('frontend.shop.shop');
    }

    public function TimKiem(Request $request) 
    {
        $data = $request->all();
              
        $q = Product::query();

        if ($request->has('name')) {
            // Sử dụng phương thức input() để lấy giá trị của tham số
            $q->where('name', 'like', '%' . $request->input('name') . '%');
        }
       if ($request->has('price')) {
        $priceRange = $request->input('price');

        // Phân tách giá trị thành mảng
        $priceArray = explode('-', $priceRange);

        // Xác nhận có đủ hai giá trị
        if (count($priceArray) == 2) {
            // Sử dụng whereBetween để lấy sản phẩm trong khoảng giá
            $q->whereBetween('price', $priceArray);
        }
        if ($request->has('category_id')) {
        $q->where('category_id', '=', $request->input('category_id'));
        }
        if ($request->has('brand_id')) {
        $q->where('brand_id', '=', $request->input('brand_id'));
        }


        // Tiếp tục xử lý tìm kiếm với các điều kiện khác nếu cần

        
        $results = $q->get();

        
        return view('frontend.shop.ketqua', compact('results'));
    }
}
// Trong hàm getProducts của controller
public function getProducts(Request $request) 
{
    $minPrice = $request->input('minPrice');
    $maxPrice = $request->input('maxPrice');

    // Lấy các sản phẩm trong khoảng giá đã chọn
    $product = Product::whereBetween('price', [$minPrice, $maxPrice])->get();



 return response()->json(['data' => $product->toArray()]);
    
}

}
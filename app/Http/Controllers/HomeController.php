<?php
namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
    
     */
    public function index()
    {
        $product = Product::orderBy('created_at', 'desc')->take(6)->get();
        
        return view('frontend/index',compact('product'));
    }
     public function showDetail($id)
    {
        // Lấy thông tin sản phẩm từ database (sử dụng Eloquent)
        $productdetail = Product::find($id);
       

        // Truyển dữ liệu sản phẩm tới view và hiển thị
        return view('frontend.productdetail.productdetail', compact('productdetail'));
    }
}

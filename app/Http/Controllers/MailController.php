<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNotify;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Auth;

class MailController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!$user) {
            // User is not authenticated, handle accordingly
            return response()->json(['message' => 'User not authenticated.']);
        }

        // User is authenticated, proceed with retrieving order details and sending email
        $orderDetails = OrderDetail::where('order_id', $user->id)->get();
        $totalPrice = OrderDetail::where('order_id', $user->id)->sum('total_price');

        $data = [
            'subject' => 'Cambo Tutorial Mail',
            'body' => 'Hello, this is my email delivery',
            'orderDetails' => $orderDetails,
            'totalPrice' => $totalPrice,
             'user' => $user,
        ];

        try {
            Mail::to('phuockhoanguyen633@gmail.com')->send(new MailNotify($data));
            return response()->json(['message' => 'Great! Check your mailbox for the invoice.']);
        } catch (\Exception $th) {
            return response()->json(['message' => 'Sorry, something went wrong.']);
        }
    }
}



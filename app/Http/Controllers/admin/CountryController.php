<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\admin\CountryController;
use App\Http\Requests\admin\CountryRequest;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
           //xu ly nhu the nay la vua phan trang vua lay du lieu luon

          $data = Country::paginate(5);

           return view('admin/country/country', compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     */
public function GetAdd()
{
    

    return view('admin/country/add');
}
 public function PostAdd(CountryRequest $request)
    {
        $country = new Country();

    // Get the name from the form
    $name = $request->input('name');
    // Set the data
    $country->name = $name;
    $country->save();
    return redirect()->back()->with('success', __('Update profile success.'));
}
//chỉn sữa
public function GetEdit($id)
{
    if (!empty($id)) {
        // Lấy dữ liệu ID được chỉ định từ cơ sỡ dữ liệu
        $data = Country::where('id', $id)->first();
        return view('admin/country/edit', compact('data'));
    } else {
        return view('country')->with('success', __('Người dùng không tồn tại'));
    }
    return view('admin/country/edit', compact('data'));
}

public function PostEdit(CountryRequest $request, $id)
{
    $country = Country::find($id);

    // Lấy dữ liệu từ request
    $country->name = $request->input('name');

    $country->save();

   return redirect()->back()->with('success', __('Update profile success.'));
}
  public function delete($id){
        Country::where('id',$id)->delete();
        return redirect()->back()->with('success', __('xóa thàng công.'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

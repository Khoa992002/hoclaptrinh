<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //muoons đá ra khỏi trang login th
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
     

    }
    public function getProfile()
    {
        // Your logic for fetching and displaying profile data goes here
        $user = Auth::user();
        $member=User::where('id',$user->id)->first();
        return view('admin/profile/profile',compact('member'));
    }


        /**
         * Show the form for creating a new resource.
         */
    public function PostProfile(Request $request)
    {
         
        $userId = Auth::id();
        $user = User::findOrFail($userId);

        $data = $request->all();
        // Lấy tất cả dữ liệu từ yêu cầu
        $file = $request->avatar;
        // Kiểm tra xem có tệp avatar được tải lên không
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }

        
        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
       
        if ($user->update($data)) {
            if(!empty($file)){
                $file->move('upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', __('Update profile success.'));
        } else {
            return redirect()->back()->withErrors('Update profile error.');
        }

        // No need to update the user instance again, as the update method directly modifies the database records
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

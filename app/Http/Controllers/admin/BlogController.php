<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Rate;
use App\Http\Requests\admin\BlogRequest;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
   //muoons đá ra khỏi trang login th
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Blog::paginate(3);
        
        return view('admin/blog/blog', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function getAdd2()
    {
        return view('admin/blog/add2');
    }

    public function postAdd2(BlogRequest $request)
    {
        // Get data from the request
        $title = $request->input('title');
        $description = $request->input('description');
        $image = $request->file('image');

        // Create a new Blog instance
        $blog = new Blog();

        // Assign data to attributes
        $blog->title = $title;
        $blog->description = $description;

        // Save image to the database
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $image->move('admin/upload/blog', $imageName);
            $blog->image = $imageName;
        }

        $blog->save();

        return redirect()->back()->with('success', __('Update profile success.'));
    }

    public function getEdit2($id)
    {
        // Check if ID is not empty
        if (!empty($id)) {
            // Get data by ID from the database
            $data = Blog::where('id', $id)->first();
            return view('admin.blog.edit2', compact('data'));
        } else {
            return view('country')->with('success', __('Người dùng không tồn tại'));
        }
    }

    public function postEdit2(BlogRequest $request, $id)
    {
        $blog = Blog::find($id);

        // Get data from request
        $blog->title = $request->input('title');
        $blog->description = $request->input('description');

        // Save new image to the database
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->move('admin/upload/blog', $imageName);
            $blog->image = $imageName;
        }

        $blog->save();

        return redirect()->back()->with('success', __('Update profile success.'));
    }

    public function delete2($id)
    {
        Blog::where('id', $id)->delete();
        return redirect()->back()->with('success', __('xóa thàng công.'));
    }

      public function rateBlog(Request $request)
    {
        // Get the data from the AJAX request
        $rate = $request->input('rate');
        $blogId = $request->input('id_blog');

        // You can perform validation and additional checks here

        // Assuming you have a 'ratings' table to store ratings
        $rating = new Rate();
        $rating->rate = $rate;
        $rating->blog_id = $id_blog;
        $rating->user_id = auth()->id(); // Assuming you're using Laravel's built-in authentication
        $rating->save();

        return response()->json(['success' => true]);
    }
}



   



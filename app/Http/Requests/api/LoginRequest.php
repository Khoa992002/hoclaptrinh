<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\api\FormRequest;


class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
   public function rules(): array
    {
        return [
            'email'=>'required',
            'password'=>'required'
            
            
            
            
        ];
    }
    public function messages()
    {
        return[
            'required'=>':attribute không được để trống',
            'max'=>':attribute không được quá :max ký tự',

        ];
    }
}

<?php

namespace App\Http\Requests\frontend\Api;

use App\Http\Requests\api\FormRequest;


class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
   public function rules(): array
    {
        return [
            'name'=>'required',
            'email'=>'required',
            'avatar'=>'required',
            'phone'=>'required|max:20',
            'address'=>'required|max:20',
            
            
            
        ];
    }
    public function messages()
    {
        return[
            'required'=>':attribute không được để trống',
            'max'=>':attribute không được quá :max ký tự',

        ];
    }
}

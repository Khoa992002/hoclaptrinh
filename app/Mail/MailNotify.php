<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailNotify extends Mailable
{
    use Queueable, SerializesModels;

    public $data; // Thêm thuộc tính $data

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->from('phuockhoanguyen633@gmail.com', "Your Name")
            ->subject($this->data['subject'])
            ->view("emails.index")
            ->with("data", $this->data);
    }
}

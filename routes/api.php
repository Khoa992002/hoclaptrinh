<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\BlogController2;
use App\Http\Controllers\Api\MemberController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
    'namespace' => 'Api'
], function () {
    //member

Route::post('member/login', [MemberController::class, 'PostLogin']);
//blog    
Route::get('/blog', [BlogController2::class, 'GetBlog'])->name('blog');
Route::get('/blog/blogshow/{id}', [BlogController2::class, 'GetDlog'])->name('blogshow');





});
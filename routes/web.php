<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Auth\LoginController;

// Admin controllers
use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\CountryController;
use App\Http\Controllers\admin\BlogController;
use App\Http\Controllers\admin\CategoryController;
use App\Http\Controllers\admin\BrandController;

// Frontend controllers
use App\Http\Controllers\frontend\MemberController;
use App\Http\Controllers\frontend\BlogController2;
use App\Http\Controllers\frontend\AcountController;
use App\Http\Controllers\frontend\ProductController;
use App\Http\Controllers\frontend\CartController;
use App\Http\Controllers\frontend\CheckoutController;
use App\Http\Controllers\frontend\ShoppController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'namespace' => 'Frontend',
], function () {
   Route::get('/', [HomeController::class, 'index'])->name('home');

   Route::get('/blog', [BlogController2::class, 'GetBlog'])->name('blog');
   Route::get('/blog/blogshow/{id}', [BlogController2::class, 'GetDlog'])->name('blogshow');
   Route::post('/blog/rate', [BlogController2::class, 'rateBlog'])->name('blog.rate');
   Route::get('/blog/repcmt/{id}', [BlogController2::class, 'RepCmt'])->name('blog.repcmt');
   Route::post('/blog/cmt', [BlogController2::class, 'PostCmt'])->name('blog.cmt');
   Route::get('/getProducts', [ShoppController::class, 'getProducts'])->name('shop');
    Route::get('/cart', [CartController::class, 'GetCart'])->name('cart');
        Route::post('/Product/Cart', [CartController::class, 'PostCart'])->name('cart.post');
        Route::post('/Cart/capnhat', [CartController::class, 'PCart'])->name('cart.capnhat');
        Route::post('/cart/remove', [CartController::class, 'RCart']);
             
        Route::get('/product/productdetail/{id}', [HomeController::class, 'showDetail'])->name('product.detail');


    // **Member** routes

    Route::group(['middleware' => 'memberNotLogin'], function () {
        Route::get('/member/register', [MemberController::class, 'GetRegister']);
        Route::post('/member/register', [MemberController::class, 'PostRegister']);

      Route::get('/member/login', [MemberController::class, 'GetLogin']);
       Route::post('/member/login', [MemberController::class, 'PostLogin']);
       
    });

    // **Home** route


    // **Member** routes (after login)
    Route::group(['middleware' => 'member'], function () {
        //product
        Route::get('/product/myproduct', [ProductController::class, 'GetProduct'])->name('product.myproduct');
        Route::get('/add', [ProductController::class, 'AddProduct'])->name('product.add');
        Route::post('/add/new', [ProductController::class, 'PostProduct'])->name('product.post');
        Route::get('/edit/{id}', [ProductController::class, 'GetEditpro'])->name('product.edit');
        Route::post('/edit/new/{id}', [ProductController::class, 'EditPro'])->name('product.edit.new');
        Route::get('/deletepro/{id}', [ProductController::class, 'Deletepro'])->name('product.delete');
        //member
        Route::get('/member/logout', [MemberController::class, 'Logout'])->name('logout');
        Route::get('/account', [AcountController::class, 'GetAccount'])->name('account');
        Route::post('/account/update', [AcountController::class, 'UpdateAccount'])->name('account.update');

         //blog

       
   
        



        });

       
   
       

       

      

       
//Route::post('/cart/remove2', [CartController::class, 'RCart2']);
/// xử lý check out
// **Checkout** route

       Route::get('/checkout', [CheckoutController::class, 'GetCheckout'])->name('checkout');

// **Mail test** route

       Route::get('/test', [MailController::class, 'index'])->name('test');

// **Search** route

       Route::get('product/search', [ProductController::class, 'GetTimkiem'])->name('product.search');

// **Shop** routes

        Route::get('/shop', [ShoppController::class, 'GetShop'])->name('shop');
        Route::post('shop/ketqua', [ShoppController::class, 'TimKiem'])->name('shop.timkiem');
      
      });
// **Admin** routes
Auth::routes();
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Auth'
], function () {
    Route::get('/',[LoginController::class, 'showLoginForm']);
    Route::get('/login',[LoginController::class, 'showLoginForm']);
    Route::post('/login',[LoginController::class, 'login']);
    Route::get('/logout',[LoginController::class, 'logout']);
   
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['admin'],
], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    Route::get('/country', [CountryController::class, 'index'])->name('admin.country');
    Route::get('/blog', [BlogController::class, 'index'])->name('admin.blog');

    // **Country** routes

    Route::get('/country/add', [CountryController::class, 'GetAdd'])->name('admin.country.add');
    Route::post('/country/add', [CountryController::class, 'PostAdd'])->name('admin.country.post');
    Route::get('/edit/{id}', [CountryController::class, 'GetEdit'])->name('admin.country.edit');
    Route::post('/edit/{id}', [CountryController::class, 'PostEdit'])->name('admin.country.postedit');
    Route::get('/delete/{id}', [CountryController::class, 'delete'])->name('admin.country.delete');

    // **Blog** routes

    Route::get('/blog/add2', [BlogController::class, 'GetAdd2'])->name('admin.blog.add2');
    Route::post('/blog/add2', [BlogController::class, 'PostAdd2'])->name('admin.blog.postadd2');
    Route::get('/edit2/{id}', [BlogController::class, 'GetEdit2'])->name('edit2');
    Route::post('/edit2/{id}', [BlogController::class, 'PostEdit2'])->name('admin.blog.postedit2');
    Route::get('/delete2/{id}', [BlogController::class, 'delete2'])->name('delete2');

    // **Profile** routes

    Route::get('/profile', [UserController::class, 'getProfile'])->name('admin.profile.get');
    Route::post('/edit/member/{id}', [UserController::class, 'PostProfile'])->name('admin.profile.post');

    // **Category** routes

    Route::get('/category', [CategoryController::class, 'getCategory'])->name('admin.category');
    Route::post('category/add', [CategoryController::class, 'PostCategory'])->name('admin.category.post');
    Route::get('/category/addCategory', [CategoryController::class, 'GetAddct'])->name('admin.category.addct');

    // **Brand** routes

    Route::get('/Brand', [BrandController::class, 'getBrand'])->name('admin.brand');
});
